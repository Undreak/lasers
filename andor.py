import pandas as pd
from scipy.signal import find_peaks
import matplotlib.pyplot as plt

sample1 = pd.read_csv('Data Andor/sample1.asc', sep='\t')
sample1.columns = ['wavelength', '1', '2', '3', '4', '5', '6']

sample2 = pd.read_csv('Data Andor/sample2.asc', sep='\t')
sample2.columns = ['wavelength', '1', '2', '3', '4', '5', '6']

sample3 = pd.read_csv('Data Andor/sample3.asc', sep='\t')
sample3.columns = ['wavelength', '1', '2', '3', '4', '5', '6']

sample4 = pd.read_csv('Data Andor/sample4.asc', sep='\t')
sample4.columns = ['wavelength', '1', '2', '3', '4', '5', '6']

al = pd.read_csv("Al reference.asc", sep='\t', names = ['wavelength', 'intensity', 'na'])

plt.figure(figsize=(10, 6))
plt.plot(sample1['wavelength'], sample1['1'], label='Sample dry 36mJ')
plt.plot(sample2['wavelength'], sample2['1'], label='Sample not dry 36mJ')
plt.plot(sample3['wavelength'], sample3['1'], label='Sample dry 55mJ')
plt.plot(sample4['wavelength'], sample4['1'], label='Sample not dry 55mJ')

# Add labels and title
plt.xlabel('Wavelength')
plt.ylabel('Intensity')
plt.title('Samples')
plt.legend()
plt.savefig('plot/samples.png', dpi=300)

# Show the plot
plt.show()

# DRY VS NOT DRY
plt.figure(figsize=(10, 6))
plt.plot(sample1['wavelength'], sample1['1'], label='Sample dry')
plt.plot(sample2['wavelength'], sample2['1'], label='Sample not dry')

# Add labels and title
plt.xlabel('Wavelength')
plt.ylabel('Intensity')
plt.title('Samples')
plt.legend()
plt.savefig('plot/sample_12_dry_notdry.png', dpi=300)

# Show the plot
plt.show()
plt.figure(figsize=(10, 6))
plt.plot(sample3['wavelength'], sample3['1'], label='Sample dry')
plt.plot(sample4['wavelength'], sample4['1'], label='Sample not dry')

# Add labels and title
plt.xlabel('Wavelength')
plt.ylabel('Intensity')
plt.title('Samples')
plt.legend()
plt.savefig('plot/sample_34_dry_notdry.png', dpi=300)

# Show the plot
plt.show()

#### ALUMINIUM 
plt.plot(sample1['wavelength'], sample1['1'], label='Sample dry 36mJ')
plt.plot(al['wavelength'], al['intensity'], label='Al reference')
# Add labels and title
plt.xlabel('Wavelength')
plt.ylabel('Intensity')
plt.title('Samples')
plt.legend()
plt.savefig('plot/samples_al.png', dpi=300)

# Show the plot
plt.show()

# Calculate peaks for component 1
peaks_sample1, _ = find_peaks(sample1['1'], prominence=2000, width=3)

# Calculate peaks for aluminum reference
peaks_al, _ = find_peaks(al['intensity'], prominence=1000, width=3)

# Plot both components
plt.figure(figsize=(10, 6))  # Adjust size as needed
plt.plot(sample1['wavelength'], sample1['1'], label='Sample 1')
plt.plot(sample1['wavelength'][peaks_sample1], sample1['1'][peaks_sample1], 'x', label='Sample 1 Peaks', color='red')

plt.plot(al['wavelength'], al['intensity'], label='Aluminum Reference')
plt.plot(al['wavelength'][peaks_al], al['intensity'][peaks_al], 'x', label='Aluminum Reference Peaks', color='blue')

# Annotate peak wavelength values for sample 1
for peak in peaks_sample1:
    plt.text(sample1['wavelength'][peak], sample1['1'][peak], f"{sample1['wavelength'][peak]:.2f}", fontsize=9, rotation=45)

# Annotate peak wavelength values for aluminum reference
for peak in peaks_al:
    plt.text(al['wavelength'][peak], al['intensity'][peak], f"{al['wavelength'][peak]:.2f}", fontsize=9, rotation=45)

plt.xlabel('Wavelength')
plt.ylabel('Intensity')
plt.title('Peaks of Component 1 and Aluminum Reference')
plt.legend()
plt.grid(True)
plt.show()

# Identify the two or three most prominent peaks
most_prominent_peaks = sorted(peaks_al, key=lambda x: al['intensity'][x], reverse=True)[:5]

# Define the zoomed ranges based on identified peaks
zoomed_ranges = [(al['wavelength'][peak] - 3, al['wavelength'][peak] + 3) for peak in most_prominent_peaks]

# Calculate peaks for sample1
peaks_sample1, _ = find_peaks(sample1['1'], prominence=2000, width=3)

# Plot each zoomed plot
for i, (start, end) in enumerate(zoomed_ranges):
    plt.figure(figsize=(10, 6))  # Adjust figsize as needed

    # Plot aluminum reference data
    plt.plot(al['wavelength'], al['intensity'], label='Al reference')

    # Plot sample1 data
    plt.plot(sample1['wavelength'], sample1['1'], label='Sample1')

    # Plot identified peaks for aluminum reference
    plt.plot(al['wavelength'][peaks_al], al['intensity'][peaks_al], 'rx', label='Peaks (Al reference)')

    # Plot identified peaks for sample1
    plt.plot(sample1['wavelength'][peaks_sample1], sample1['1'][peaks_sample1], 'bx', label='Peaks (Sample1)')

    # Annotate peaks with their wavelength values for aluminum reference
    for peak_idx in most_prominent_peaks:
        plt.annotate(f"{al['wavelength'][peak_idx]:.2f}", (al['wavelength'][peak_idx], al['intensity'][peak_idx]), textcoords="offset points", xytext=(-10,10), ha='center')

    # Annotate peaks with their wavelength values for sample1
    for peak_idx in peaks_sample1:
        plt.annotate(f"{sample1['wavelength'][peak_idx]:.2f}", (sample1['wavelength'][peak_idx], sample1['1'][peak_idx]), textcoords="offset points", xytext=(-10,10), ha='center')

    # Set labels and title
    plt.xlabel('Wavelength')
    plt.ylabel('Intensity')
    plt.title(f'Zoomed Plot {i+1}')

    # Add legend
    plt.legend()

    # Set x-axis limits based on zoomed range
    plt.xlim(start, end)

    # Show plot
    plt.grid(True)
    plt.tight_layout()
    plt.show()

# Calculate peaks for aluminum reference
peaks_al, _ = find_peaks(al['intensity'], prominence=1000, width=3)

# Identify the three most prominent peaks for aluminum reference
most_prominent_peaks = sorted(peaks_al, key=lambda x: al['intensity'][x], reverse=True)[:3]

# Define the zoomed ranges based on identified peaks
zoomed_ranges = [(al['wavelength'][peak] - 3, al['wavelength'][peak] + 3) for peak in most_prominent_peaks]

def plot_zoomed(sample_name, sample, peaks_sample, start, end):
    plt.figure(figsize=(10, 6))  # Adjust figsize as needed

    # Plot aluminum reference data
    plt.plot(al['wavelength'], al['intensity'], label='Al reference')

    # Plot sample data
    plt.plot(sample['wavelength'], sample['1'], label=f'Sample {sample_name}')

    # Plot identified peaks for aluminum reference
    plt.plot(al['wavelength'][peaks_al], al['intensity'][peaks_al], 'rx', label='Peaks (Al reference)')

    # Plot identified peaks for sample
    plt.plot(sample['wavelength'].iloc[peaks_sample], sample['1'].iloc[peaks_sample], 'bx', label=f'Peaks (Sample {sample_name})')

    # Annotate peaks with their wavelength values for sample
    for peak_idx in peaks_sample:
        plt.annotate(f"{sample['wavelength'].iloc[peak_idx]:.2f}", (sample['wavelength'].iloc[peak_idx], sample['1'].iloc[peak_idx]), textcoords="offset points", xytext=(-10,10), ha='center')
    # Annotate peaks with their wavelength values for aluminum reference
    for peak_idx in most_prominent_peaks:
        plt.annotate(f"{al['wavelength'][peak_idx]:.2f}", (al['wavelength'][peak_idx], al['intensity'][peak_idx]), textcoords="offset points", xytext=(-10,10), ha='center')

    # Set labels and title
    plt.xlabel('Wavelength')
    plt.ylabel('Intensity')
    plt.title(f'Zoomed Plot')

    # Add legend
    plt.legend()

    # Set x-axis limits based on zoomed range
    plt.xlim(start, end)

    # Show plot
    plt.grid(True)
    plt.tight_layout()
    # Save plot
    filename = f"plot/andor_zoom_{sample_name}_{start:.0f}-{end:.0f}.png"
    plt.savefig(filename)
    plt.close()

# Calculate peaks for each sample and plot the zoomed plots
for sample_name, (sample, peaks_sample) in enumerate([(sample1, find_peaks(sample1['1'], prominence=2000, width=3)),
                                                      (sample2, find_peaks(sample2['1'], prominence=2000, width=3)),
                                                      (sample3, find_peaks(sample3['1'], prominence=2000, width=3)),
                                                      (sample4, find_peaks(sample4['1'], prominence=2000, width=3))], start=1):
    for start, end in zoomed_ranges:
        plot_zoomed(sample_name, sample, peaks_sample[0], start, end)  # Assuming only one set of peaks is returned

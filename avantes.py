import pandas as pd
import matplotlib.pyplot as plt

avantes = pd.read_csv("Data Avantes/data avantes.csv", sep='.')
ca = pd.read_csv('Data Avantes/ca.csv')
na = pd.read_csv('Data Avantes/na.csv')
br = pd.read_csv('Data Avantes/br.csv')
i = pd.read_csv('Data Avantes/i.csv')

plt.figure(figsize=(10, 6), dpi=200)
plt.plot(avantes['wavelength'], avantes['ZnI2Alg'], label='ZnI2Alg')
plt.plot(avantes['wavelength'], avantes['NiBr2Alg'], label='NiBr2Alg')
# Add labels and title
plt.xlabel('Wavelength')
plt.ylabel('Intensity')
plt.title('Spectral Data')
plt.legend()
plt.savefig('plot/avantes.png', dpi=300)
# Show the plot
plt.show()

# Plot the ZnI2Alg and NiBr2Alg data
fig, ax1 = plt.subplots(figsize=(14, 8), dpi=150)
ax1.plot(avantes['wavelength'], avantes['ZnI2Alg'], color='red', label='ZnI2Alg')
ax1.set_xlabel('Wavelength (nm)')
ax1.set_ylabel('Intensity (ZnI2Alg)', color='red')
ax1.tick_params(axis='y', labelcolor='red')

ax2 = ax1.twinx()
ax2.plot(avantes['wavelength'], avantes['NiBr2Alg'], color='green', label='NiBr2Alg')
ax2.set_ylabel('Intensity (NiBr2Alg)', color='green')
ax2.tick_params(axis='y', labelcolor='green')

# Plot the 'Sum' column from the 'na' dataframe
ax3 = ax1.twinx()
ax3.plot(na['Wavelength (nm)'], na['Sum'], color='blue', label='Na')
ax3.spines['right'].set_position(('outward', 60))
ax3.set_ylabel('Intensity (Na)', color='blue')
ax3.tick_params(axis='y', labelcolor='blue')

# Plot the 'Sum' column from the 'ca' dataframe
ax4 = ax1.twinx()
ax4.plot(ca['Wavelength (nm)'], ca['Sum'], color='orange', label='Ca')
ax4.spines['right'].set_position(('outward', 120))
ax4.set_ylabel('Intensity (Ca)', color='orange')
ax4.tick_params(axis='y', labelcolor='orange')

# Adjust the spacing between axes
fig.subplots_adjust(right=0.8)

# Add a legend
lines = [ax1.get_lines()[0], ax2.get_lines()[0], ax3.get_lines()[0], ax4.get_lines()[0]]
labels = [line.get_label() for line in lines]
ax1.legend(lines, labels, loc='upper left')

plt.savefig('plot/avantes_Na_Ca.png', dpi=300)
# Show the plot
plt.show()

# Plot the ZnI2Alg and NiBr2Alg data
fig, ax1 = plt.subplots(figsize=(14, 8), dpi=150)
ax1.plot(avantes['wavelength'], avantes['ZnI2Alg'], color='red', label='ZnI2Alg')
ax1.set_xlabel('Wavelength (nm)')
ax1.set_ylabel('Intensity (ZnI2Alg)', color='red')
ax1.tick_params(axis='y', labelcolor='red')

ax2 = ax1.twinx()
ax2.plot(avantes['wavelength'], avantes['NiBr2Alg'], color='green', label='NiBr2Alg')
ax2.set_ylabel('Intensity (NiBr2Alg)', color='green')
ax2.tick_params(axis='y', labelcolor='green')

# Plot the column from the 'br' dataframe
ax3 = ax1.twinx()
ax3.plot(br['Wavelength (nm)'], br['Br I (5.7e-01)'], color='blue', label='Br')
ax3.spines['right'].set_position(('outward', 60))
ax3.set_ylabel('Intensity (Br)', color='blue')
ax3.tick_params(axis='y', labelcolor='blue')

# Plot the column from the 'i' dataframe
ax4 = ax1.twinx()
ax4.plot(i['Wavelength (nm)'], i['I I (2.7e-01)'], color='orange', label='I')
ax4.spines['right'].set_position(('outward', 120))
ax4.set_ylabel('Intensity (I)', color='orange')
ax4.tick_params(axis='y', labelcolor='orange')

# Adjust the spacing between axes
fig.subplots_adjust(right=0.8)

# Add a legend
lines = [ax1.get_lines()[0], ax2.get_lines()[0], ax3.get_lines()[0], ax4.get_lines()[0]]
labels = [line.get_label() for line in lines]
ax1.legend(lines, labels, loc='upper left')

plt.savefig('plot/avantes_Br_I.png', dpi=300)
# Show the plot
plt.show()

# Plot the data
fig, ax1 = plt.subplots(figsize=(12, 6))
ax1.plot(avantes['wavelength'], avantes['ZnI2Alg'], color='red', label='ZnI2Alg')
ax1.set_xlabel('Wavelength')
ax1.set_ylabel('Intensity (ZnI2Alg)', color='red')
ax1.tick_params(axis='y', labelcolor='red')

ax2 = ax1.twinx()
ax2.plot(avantes['wavelength'], avantes['NiBr2Alg'], color='green', label='NiBr2Alg')
ax2.set_ylabel('Intensity (NiBr2Alg)', color='green')
ax2.tick_params(axis='y', labelcolor='green')

ax3 = ax1.twinx()
ax3.plot(avantes['wavelength'], avantes['Ni'], color='blue', label='Ni')
ax3.spines['right'].set_position(('outward', 60))
ax3.set_ylabel('Intensity (Ni)', color='blue')
ax3.tick_params(axis='y', labelcolor='blue')

ax4 = ax1.twinx()
ax4.plot(avantes['wavelength'], avantes['Zn'], color='orange', label='Zn')
ax4.spines['right'].set_position(('outward', 120))
ax4.set_ylabel('Intensity (Zn)', color='orange')
ax4.tick_params(axis='y', labelcolor='orange')

# Adjust the spacing between axes
fig.subplots_adjust(right=0.8)

# Add a legend
lines = [ax1.get_lines()[0], ax2.get_lines()[0], ax3.get_lines()[0], ax4.get_lines()[0]]
labels = [line.get_label() for line in lines]
ax1.legend(lines, labels, loc='upper left')

plt.savefig('plot/avantes_Ni_Zn.png', dpi=300)
# Show the plot
plt.show()

# Plot Zn and the gels
fig, ax1 = plt.subplots(figsize=(10, 6))
ax1.plot(avantes['wavelength'], avantes['Zn'], color='red', label='Zn')
ax1.set_xlabel('Wavelength')
ax1.set_ylabel('Intensity (Zn)', color='red')
ax1.tick_params(axis='y', labelcolor='red')

ax2 = ax1.twinx()
ax2.plot(avantes['wavelength'], avantes['ZnI2Alg'], color='green', label='ZnI2Alg')
ax2.set_ylabel('Intensity (ZnI2Alg)', color='green')
ax2.tick_params(axis='y', labelcolor='green')

ax3 = ax1.twinx()
ax3.plot(avantes['wavelength'], avantes['NiBr2Alg'], color='blue', label='NiBr2Alg')
ax3.spines['right'].set_position(('outward', 60))
ax3.set_ylabel('Intensity (NiBr2Alg)', color='blue')
ax3.tick_params(axis='y', labelcolor='blue')

# Adjust the spacing between axes
fig.subplots_adjust(right=0.8)

# Add a legend
lines = [ax1.get_lines()[0], ax2.get_lines()[0], ax3.get_lines()[0]]
labels = [line.get_label() for line in lines]
ax1.legend(lines, labels, loc='upper left')

plt.savefig('plot/avantes_Zn.png', dpi=300)
# Show the plot
plt.show()

# Plot Ni and the gels
fig, ax1 = plt.subplots(figsize=(10, 6))
ax1.plot(avantes['wavelength'], avantes['Ni'], color='red', label='Ni')
ax1.set_xlabel('Wavelength')
ax1.set_ylabel('Intensity (Ni)', color='red')
ax1.tick_params(axis='y', labelcolor='red')

ax2 = ax1.twinx()
ax2.plot(avantes['wavelength'], avantes['ZnI2Alg'], color='green', label='ZnI2Alg')
ax2.set_ylabel('Intensity (ZnI2Alg)', color='green')
ax2.tick_params(axis='y', labelcolor='green')

ax3 = ax1.twinx()
ax3.plot(avantes['wavelength'], avantes['NiBr2Alg'], color='blue', label='NiBr2Alg')
ax3.spines['right'].set_position(('outward', 60))
ax3.set_ylabel('Intensity (NiBr2Alg)', color='blue')
ax3.tick_params(axis='y', labelcolor='blue')

# Adjust the spacing between axes
fig.subplots_adjust(right=0.8)

# Add a legend
lines = [ax1.get_lines()[0], ax2.get_lines()[0], ax3.get_lines()[0]]
labels = [line.get_label() for line in lines]
ax1.legend(lines, labels, loc='upper left')

plt.savefig('plot/avantes_Ni.png', dpi=300)
# Show the plot
plt.show()


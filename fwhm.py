import pandas as pd
import numpy as np
from scipy.signal import find_peaks
import matplotlib.pyplot as plt

# Import data
data = pd.read_csv("Al reference.asc", sep='\t', names = ['wavelength', 'intensity', 'na'])
data = data.drop('na', axis = 1)
libs = pd.read_csv("Al LIBS.csv")

def calculate_fwhm(data, x_col='wavelength', y_col='intensity', prominence=0.5, width=10, plot=False, filename="plot"):
    """
    Calculate the Full Width Half Maximum (FWHM) of peaks in the given data.

    Args:
        data (pandas.DataFrame): The data frame containing the curve data.
        x_col (str, optional): The name of the column containing the x-values (wavelength).
        y_col (str, optional): The name of the column containing the y-values (intensity).
        prominence (float, optional): The prominence value for peak finding.
        width (float, optional): The width value for peak finding.
        plot (bool, optional): Whether to plot the data and visualize the FWHM.

    Returns:
        list: A list of FWHM values for each peak in the curve.
    """
    # Find the peaks in the data
    peaks, properties = find_peaks(data[y_col], prominence=prominence, width=width)

    # Initialize an empty list to store FWHM values
    fwhm_values = []

    # Iterate over the peaks
    for peak_idx in peaks:
        # Find the maximum y-value for the current peak
        peak_y_max = data[y_col].iloc[peak_idx]

        # Find the half-maximum value for the current peak
        half_max = peak_y_max / 2

        # Find the indices where the y-values cross the half-maximum value
        left_crossing = np.where(data[y_col].iloc[:peak_idx] < half_max)[0]
        right_crossing = np.where(data[y_col].iloc[peak_idx:] < half_max)[0]

        # Check if there are crossing indices for the current peak
        if len(left_crossing) > 0 and len(right_crossing) > 0:
            # Find the closest crossing indices to the peak
            left_idx = left_crossing[-1]
            right_idx = peak_idx + right_crossing[0]

            # Calculate the FWHM for the current peak
            fwhm = data[x_col].iloc[right_idx] - data[x_col].iloc[left_idx]
            fwhm_values.append(fwhm)

    if plot:
        # Plot the original data
        plt.figure(figsize=(10, 6))
        plt.plot(data[x_col], data[y_col], color='lightblue', label='Original Data')

        # Plot the peak locations
        plt.scatter(data[x_col].iloc[peaks], data[y_col].iloc[peaks], color='lightcoral', label='Peaks')

        # Plot the FWHM for each peak
        for i, fwhm in enumerate(fwhm_values):
            peak_x = data[x_col].iloc[peaks[i]]
            peak_y = data[y_col].iloc[peaks[i]]
            half_max_y = peak_y / 2
            left_x = peak_x - fwhm / 2
            right_x = peak_x + fwhm / 2
            plt.hlines(half_max_y, left_x, right_x, color='orange', linestyles='--', label=f'FWHM {i+1}' if i == 0 else "")

        plt.xlabel('Wavelength')
        plt.ylabel('Intensity')
        plt.title('Data and FWHM Visualization')
        plt.legend()
        plt.savefig(f"plot/{filename}.png")
        plt.show()

    return fwhm_values, peaks, properties

# Assuming your data frame is called 'data'
fwhm_values, peaks, properties = calculate_fwhm(data, x_col='wavelength', y_col='intensity', prominence=1000, width=3, plot=True, filename="fwhm")

# Print the FWHM values
for i, fwhm in enumerate(fwhm_values):
    print(f"The FWHM of peak {i + 1} is: {fwhm}")

calculate_fwhm(data[15200:15400], x_col='wavelength', y_col='intensity', prominence=1000, width=3, plot=True, filename="zoom")

calculate_fwhm(libs, x_col='Wavelength (nm)', y_col='Al I (4.9e-2)', prominence=1000, width=3, plot=True, filename="libsI")

calculate_fwhm(libs, x_col='Wavelength (nm)', y_col='Al II (9.5e-1)', prominence=1000, width=3, plot=True, filename="libsII")

calculate_fwhm(libs, x_col='Wavelength (nm)', y_col='Al III (7.0e-4)', prominence=1000, width=3, plot=True, filename="libsIII")

